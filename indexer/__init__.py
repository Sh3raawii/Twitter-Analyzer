import datetime
from flask import Blueprint
from adapters.elastic_adapter import ElasticAdapter

doc_indexer = Blueprint('doc_indexer', __name__)

user_tweets_index_name = "user_tweets"
user_tweets_doc_name = "tweet"
user_tones_index_name = "user_tones"
user_tones_doc_name = "tones"
user_tones_index_mapping = {
    "mappings": {
            "tones": {
                "properties": {
                    "screen_name": {
                        "type": "keyword"
                    },
                    "user_id": {
                        "type": "keyword"
                    },
                    "agg_tones": {
                        "properties": {
                            "name": {
                                "type": "keyword"
                            },
                            "value": {
                                "type": "double"
                            }
                        }
                    }
                }
            }
        }
    }


@doc_indexer.before_app_first_request
def create_all_indices():
    ElasticAdapter.create_index(user_tones_index_name, user_tones_index_mapping)


def index_users_tones(users_tones):
    users_tones = set_doc_id(users_tones, "user_id")
    ElasticAdapter.bulk_index(user_tones_index_name, user_tones_doc_name, users_tones)


def index_user_tones(user_tones):
    document_id = user_tones.get("user_id")
    ElasticAdapter.index(user_tones_index_name, user_tones_doc_name, document_id, user_tones)


def retrieve_tones_for_user(screen_name):
    results = ElasticAdapter.search(user_tones_index_name, user_tones_doc_name,
                                    query={"query": {"term": {"screen_name": screen_name}}})
    results = results['hits']['hits']
    user_tones = None
    if len(results) > 0:
        user_tones = results[0]['_source']
        user_tones = user_tones.get("agg_tones")
    return user_tones


def get_all_user_tweets(user_id):
    for tweet in ElasticAdapter.scan(user_tweets_index_name, user_tweets_doc_name,
                                     query={"query": {"term": {"user_id": user_id}}}):
        # convert created_at from str to python datetime
        tweet = tweet["_source"]
        tweet['created_at'] = datetime.datetime.strptime(tweet['created_at'], "%Y-%m-%dT%H:%M:%S")
        yield tweet


def set_doc_id(doc_reader, id_field_name):
    """
    set the _id field to avoid duplicate documents
    :param doc_reader: list/generator of documents
    :param id_field_name: field name with unique value
    :return: doc
    """
    for doc in doc_reader:
        doc['_id'] = doc.get(id_field_name)
        yield doc
