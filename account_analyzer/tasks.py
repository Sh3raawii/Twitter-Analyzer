from . import create_app
from .celery import create_celery
from config import get_app_config
from adapters.watson import WatsonToneAnalyzerAdapter
from indexer import get_all_user_tweets, index_user_tones
from collections import defaultdict
from statistics import mean


celery = create_celery(create_app(get_app_config(), celery=True))


@celery.task
def calc_user_tones(user):
    """fetch all tweets from elastic search and calculate the user tones"""
    watson_tone_analyzer = WatsonToneAnalyzerAdapter()
    tones = defaultdict(list)
    tweets = list()
    # now the watson api processes no more than 1000 sentences and ignores the rest
    # i think on average there are like 5 sentences per tweet so i am going to concat every 200 tweets
    for i, tweet in enumerate(get_all_user_tweets(user_id=user.get("user_id"))):
        tweets.append(tweet['text'])
        if (i + 1) % 200 == 0:
            tweets_text = '. '.join(tweets)
            for tone in watson_tone_analyzer.document_tones(tweets_text):
                tone_name = tone.get('tone_name')
                tones[tone_name].append(tone.get('score'))
    # if less than 200
    if len(tweets) != 0:
        tweets_text = '. '.join(tweets)
        for tone in watson_tone_analyzer.document_tones(tweets_text):
            tone_name = tone.get('tone_name')
            tones[tone_name].append(tone.get('score'))

    user_tones = dict()
    user_tones["user_id"] = user.get("user_id")
    user_tones["screen_name"] = user.get("screen_name")
    user_tones["agg_tones"] = list()
    # aggregate tones
    for key, value in tones.items():
        user_tones["agg_tones"].append({
            "name": key,
            "value": mean(value)
        })
    index_user_tones(user_tones)
    return "done for user: {}".format(user.get("screen_name"))
