from flask import Flask
from config import get_app_config
from .views import analyzer
from indexer import doc_indexer
from broker import start_consumer


def create_app(app_config=None, celery=False):
    app = Flask(__name__)

    # setting app config
    app_config = app_config if app_config else get_app_config()
    app.config.from_object(app_config)

    # register blueprints
    app.register_blueprint(analyzer)
    app.register_blueprint(doc_indexer)

    # start consumer
    if not celery:
        start_consumer(app)

    return app
