from flask import jsonify
from flask import Blueprint
from indexer import retrieve_tones_for_user


analyzer = Blueprint('analyzer', __name__)


@analyzer.route('/<username>', methods=['GET'])
def get_emotional_tones(username):
    tones = retrieve_tones_for_user(username)
    if tones:
        return jsonify(tones), 200
    else:
        return '', 202
