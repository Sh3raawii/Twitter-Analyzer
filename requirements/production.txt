celery==4.2.1
elasticsearch==6.3.0
Flask==1.0.2
pika==0.12.0
requests==2.19.1
watson-developer-cloud==1.5.0
