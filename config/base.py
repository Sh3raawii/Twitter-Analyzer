import os


class BaseConfig:
    ENV = "development"
    DEBUG = True
    BASE_DIR = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
    SECRET_KEY = "development_key"

    # IBM Tone Analyzer Lite Service Credentials
    IBM_TONE_ANALYZER_URL = "https://gateway.watsonplatform.net/tone-analyzer/api"
    IBM_TONE_ANALYZER_USERNAME = "28b9f2bd-2da0-4b09-a753-3b6f295801af"
    IBM_TONE_ANALYZER_PASSWORD = "n2nbZ8NnfN6m"

    # Celery Configurations
    CELERY_BROKER_URL = "amqp://wuzzuf2:password@localhost/app2"
    CELERY_RESULT_BACKEND = "amqp"

    # Message Broker Configurations
    MESSAGE_BROKER_URL = "localhost"
