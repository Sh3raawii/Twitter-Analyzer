import os
from .base import BaseConfig


class ProductionConfig(BaseConfig):
    ENV = "production"
    DEBUG = False
    SECRET_KEY = os.environ.get("SECRET_KEY")

    # IBM Tone Analyzer Service Credentials
    IBM_TONE_ANALYZER_URL = os.environ.get("IBM_TONE_ANALYZER_URL")
    IBM_TONE_ANALYZER_USERNAME = os.environ.get("IBM_TONE_ANALYZER_USERNAME")
    IBM_TONE_ANALYZER_PASSWORD = os.environ.get("IBM_TONE_ANALYZER_PASSWORD")

    # Celery Configurations
    CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL")
    CELERY_RESULT_BACKEND = os.environ.get("CELERY_RESULT_BACKEND")

    # Message Broker Configurations
    MESSAGE_BROKER_URL = os.environ.get("MESSAGE_BROKER_URL")
