from flask import current_app
from watson_developer_cloud import ToneAnalyzerV3


class WatsonToneAnalyzerAdapter:
    def __init__(self):
        self.url = current_app.config.get("IBM_TONE_ANALYZER_URL")
        # self.url = "https://gateway.watsonplatform.net/tone-analyzer/api"
        self.username = current_app.config.get("IBM_TONE_ANALYZER_USERNAME")
        # self.username = "28b9f2bd-2da0-4b09-a753-3b6f295801af"
        self.password = current_app.config.get("IBM_TONE_ANALYZER_PASSWORD")
        # self.password = "n2nbZ8NnfN6m"
        self.tone_analyzer = ToneAnalyzerV3(url=self.url, version="2017-09-21",
                                            username=self.username, password=self.password)

    def document_tones(self, text, content_type='application/json'):
        result = self.tone_analyzer.tone({"text": text}, content_type=content_type)
        return result['document_tone']['tones']


if __name__ == '__main__':
    tones = WatsonToneAnalyzerAdapter().document_tones('Team, I know that times are tough! '
                                                       'Product sales have been disappointing '
                                                       'for the past three quarters. '
                                                       'We have a competitive product, '
                                                       'but we need to do a better job of selling it!')
    print(type(tones), tones)
